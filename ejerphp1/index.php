<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Ejercicio 1</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
		<link href="https://fonts.googleapis.com/css?family=Roboto|Roboto+Mono" rel="stylesheet">
    </head>
    <body>
        <div class="container text-center mb-3">
        	<h1 class="h1">Ejercicio número 1</h1>
        </div>
        <div class="container mb-3 ">
        	<div class="row">
        		<div class="col text-center">
        			<p>Calcular la suma de los números pares e impares del 1 al 100 y comparar cual de las dos sumas es mayor.
        			</p>
        		</div>
        	</div>
        </div>
        <section>
        	<?php 

        	$acumPar = 0;
        	$acumInpar = 0;
        	$nMayor = 0;
        	for ($i=1; $i < 100 ; $i++) { 
    			if ($i%2 == 0) {
					$acumPar += $i;
				}else{
					$acumInpar += $i;
				}
    		}
        	if ($acumPar > $acumInpar) {
        		$nMayor = $acumPar;
        	}else{
        		$nMayor = $acumInpar;
        	}
        ?>
        	<div class="container text-center">
        		<div class="row">
        			<div class="col-12 col-md-6">
        				<p>La suma de los números pares es de: </p>
        			</div>
        			<div class="col-12 col-md-6" id="sumaPar">
        				<input type="text" name="numPar" readonly="" value="<?php echo $acumPar; ?>">
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-12 col-md-6">
        				<p>La suma de los números impares es de: </p>
        			</div>
        			<div class="col-12 col-md-6" id="sumaInpar">
        				<input type="text" name="numInpar" readonly="" value="<?php echo $acumInpar;?>">
        			</div>
        		</div>
        		<div class="row">
        			<div class="col-12 col-md-6">
        				<p>La suma mayor es :</p>
        			</div>
        			<div class="col-12 col-md-6" id="sumaMay">
        				<input type="text" name="nMayor" readonly="" value="<?php echo $nMayor; ?>">
        			</div>
        		</div>
        	</div>
        	
        </section>
        
    </body>
</html>